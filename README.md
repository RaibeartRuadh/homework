
Работы выполнялись на ОС Ubuntu 16 
При выполнении работ, придерживался инструкций и рекомендаций
https://github.com/dmitry-lyutenko/manual_kernel_update/blob/master/manual/manual.md

1. Выполнить установку пакетов приложений
sudo apt install git curl

2. Установка vagrant 2.2.6
Выбрать на сайте https://releases.hashicorp.com
версию Linux 2.2.6 для x64 
ссылка https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_linux_amd64.zip

Скачать и становить через команду:

$ curl -O https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_linux_amd64.zip
$ sudo dpkg -i vagrant_2.2.9_linux_amd64.zip

Перейти на сайт Oracle VirtualBox и скачать версию 6.0 для Ubuntu 16

$ wget https://download.virtualbox.org/virtualbox/6.0.24/virtualbox-6.0_6.0.24-139119~Ubuntu~xenial_amd64.deb
$ sudo dpkg -i virtualbox-6.0_6.0.24-139119_Ubuntu_xenial_amd64.deb

Выполнить установку  packer 1.4.4 для x64

$ curl https://releases.hashicorp.com/packer/1.4.4/packer_1.4.4_linux_amd64.zip 
$ sudo gzip -d > /usr/local/bin/packer
$ sudo chmod +x /usr/local/bin/packer

Перейти по адресу https://github.com/dmitry-lyutenko/manual_kernel_update на GitHub
и выполнить fork ветки в свой репозиторий

Выгрузить к себе пакет с данными
$ git clone https://github.com/RaibeartRuadh/manual_kernel_update.git

Далее нужно выполнить пункты
kernel update и grub2 update из руководства
https://github.com/dmitry-lyutenko/manual_kernel_update/blob/master/manual/manual.md

Итогом должно быть обновленное ядро.
Получить информация по команде:
$ uname -r

Packer. Создаем свою сборку *.box с обновленноем ядром

В подготовленном файле centos.json
при проверке потребовалось внести исправления:

- заменить 
"iso_url": "https://mirror.yandex.ru/centos/7.8.2003/isos/x86_64/CentOS-7-x86_64-Minimal-2003.iso",
"iso_checksum": "659691c28a0e672558b003d223f83938f254b39875ee7559d1a4a14c79173193",
- Убрать из centos.json параметр sha_type как уже устаревший для packer
Проверить, что скрипты, выполняющие операцию по обновлению ядра и grub есть по указанному адресу. 
выполнить сборку

$ packer build centos.json

Результатом станет файл сборки centos-7.7.1908-kernel-5-x86_64-Minimal.box

Выполним загрузку результата на удаленный репозиторий Vagrant Cloud
При авторизации в VagrantCloud важно учитывать 3 параметра, логин, пароль и токен. Токен можно получить в настройках безопасности https://app.vagrantup.com/settings/security

При добавлении в VagrantCloud важно указать название релиза Kernel7, номер сборки 1 и не забыть опубликовать

Проверка сборки:

Создать директорию и перейти в нее
 Выполнить команду:
 $ vagrant init RaibeartRuadh/Kernel7 --box-version 1
 $ vagrant up
 
 В результате чего будет выгружен box и запущена виртуальная машина.
 
 
 

Пришлось добавить результат в bitbucket так как github более не поддерживает загрузку файлов бюолее 100 Мб.




